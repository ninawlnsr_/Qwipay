package com.example.baka57r.ezpy;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by nina on 02/07/2019.
 */

public interface NotifApi {
    String BASE_URL = "http://10.10.29.181:3040/api/v1/";

    //@FormUrlEncoded
    @GET("notification")
    Call<ResponseBody> getNotif(@Query("role") String role, @Query("email") String email, @Header("Authorization") String auths);
}





















