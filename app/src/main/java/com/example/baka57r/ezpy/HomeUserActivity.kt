package com.example.baka57r.ezpy

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*
import android.widget.Toast
import android.widget.AdapterView
import android.widget.TextView
import kotlinx.android.synthetic.main.app_bar.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import org.json.JSONArray



class HomeUserActivity : AppCompatActivity() {

    internal var images = intArrayOf(R.drawable.iconscan, R.drawable.icon_top_up,
            R.drawable.iconriwayat, R.drawable.icon_account)

    internal var title = arrayOf("Scan", "Isi saldo", "Riwayat", "QID")
    internal lateinit var value1: String
    internal lateinit var value3:String
    internal lateinit var value2:String
    internal lateinit var value4:String
    internal lateinit var value5:String

    internal lateinit var hasil1: String
    internal lateinit var hasil2:String
    internal lateinit var hasil3:String
    internal lateinit var hasil4:String
    internal lateinit var hasil5:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val bundle = intent.extras

        value1 = bundle!!.getString("param1")
        value2 = bundle.getString("param2")
        value3 = bundle.getString("param3")
        value4 = bundle.getString("param4")
        value5 = bundle.getString("param5")

        notif_button.setOnClickListener {

        }

        val retrofit = Retrofit.Builder().baseUrl(SaldoApi.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()

        val api = retrofit.create(SaldoApi::class.java)

        val call = api.getSaldo(value4, "Bearer $value2")
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val jsonRes = JSONObject(response.body()!!.string())

                        hasil1 = jsonRes.getString("_id")
                        hasil2 = jsonRes.getString("email")
                        hasil3 = jsonRes.getString("name")
                        hasil4 = jsonRes.getString("jumlah_uang")
                        hasil5 = jsonRes.getString("__v")

                        user_name.setText(hasil3)
                        user_saldo.setText(hasil4)

                    } catch (e: JSONException) {
                        val tost1 = Toast.makeText(applicationContext, "Error", Toast.LENGTH_LONG)
                        tost1.show()
                        e.printStackTrace()
                    } catch (e: IOException) {
                        val tost2 = Toast.makeText(applicationContext, "Error", Toast.LENGTH_LONG)
                        tost2.show()
                        e.printStackTrace()
                    }

                } else {
                    //loading.dismiss();
                    val tostt = Toast.makeText(applicationContext, "Error", Toast.LENGTH_LONG)
                    tostt.show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val tost = Toast.makeText(applicationContext, "Please check your connection", Toast.LENGTH_LONG)
                tost.show()
            }
        })

        val adapter = HomeAdapter(this, title, images)
        gridviewHome.adapter = adapter

        gridviewHome.setOnItemClickListener(AdapterView.OnItemClickListener { parent, v, position, id -> Toast.makeText(this@HomeUserActivity, "Image Position: $position", Toast.LENGTH_SHORT).show() })

        gridviewHome.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val intent: Intent
            when (position) {
                0 -> {
                    intent = Intent(this, ScanPembeli::class.java)
                    intent.putExtra("param1", value1)
                    intent.putExtra("param2", value2)
                    intent.putExtra("param3", value3)
                    intent.putExtra("param4", value4)
                    intent.putExtra("param5", value5)
                    startActivity(intent)
                }
                1 -> {
                    intent = Intent(this, InputTopup::class.java)
                    intent.putExtra("param1", value1)
                    intent.putExtra("param2", value2)
                    intent.putExtra("param3", value3)
                    intent.putExtra("param4", value4)
                    intent.putExtra("param5", value5)
                    startActivity(intent)
                }
                2 -> {
                    intent = Intent(this, LogPembeli::class.java)
                    intent.putExtra("param1", value1)
                    intent.putExtra("param2", value2)
                    intent.putExtra("param3", value3)
                    intent.putExtra("param4", value4)
                    intent.putExtra("param5", value5)
                    startActivity(intent)
                }
                else -> {
                    intent = Intent(this, Profil::class.java)
                    intent.putExtra("param1", value1)
                    intent.putExtra("param2", value2)
                    intent.putExtra("param3", value3)
                    intent.putExtra("param4", value4)
                    intent.putExtra("param5", value5)
                    startActivity(intent)
                }

            }
        }

        val apiLog = retrofit.create(NotifApi::class.java)
        val call_apilog = apiLog.getNotif(value3, value4, "Bearer $value2")
//        Toast.makeText(this@HomeUserActivity, "api $call_apilog", Toast.LENGTH_LONG).show()
        call_apilog.enqueue(object : Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response!!.isSuccessful) {

                    try {
                        val jsonRes = JSONObject(response.body()!!.string())
                        val tesss = jsonRes.getString("type")

                        Toast.makeText(this@HomeUserActivity, "type $tesss", Toast.LENGTH_LONG).show()

//                        val jArray = jr.getJSONArray("content")
//                        val length = jArray.length()
//                        if (length > 0) {
//                            val recipients = arrayOfNulls<String>(length)
//                            for (i in 0 until length) {
//                                recipients[i] = jArray.getString(i)
//                            }
//
//                            Log.i("TAG", "APILOG $recipients")
//
//                        }

//                        for (i in 0 until lenght) {
//                            val tes = jArray.getJSONObject(i).getString("message")
//                            val tost3 = Toast.makeText(applicationContext, "tes $tes", Toast.LENGTH_LONG)
//                            tost3.show()
//
//                        }

                    } catch (e: JSONException) {
                        val tost1 = Toast.makeText(applicationContext, "Error1", Toast.LENGTH_LONG)
                        tost1.show()
                        e.printStackTrace()
                    } catch (e: IOException) {
                        val tost2 = Toast.makeText(applicationContext, "Error2", Toast.LENGTH_LONG)
                        tost2.show()
                        e.printStackTrace()
                    }

                } else {
                    //loading.dismiss();
                    val tostt = Toast.makeText(applicationContext, "Error3", Toast.LENGTH_LONG)
                    tostt.show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                val tost = Toast.makeText(applicationContext, "Please check your connection", Toast.LENGTH_LONG)
                tost.show()
            }

        })
    }

}