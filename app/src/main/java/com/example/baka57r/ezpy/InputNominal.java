package com.example.baka57r.ezpy;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.example.baka57r.ezpy.utils.TextView_Lato;

public class InputNominal extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView_Lato tittle;
    private ImageButton buttonFAQ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_harga);
        toolbar = (Toolbar) findViewById(R.id.toolbar_ic);
        tittle = (TextView_Lato) findViewById(R.id.titleSearch);
        tittle.setText("Transaksi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        buttonFAQ = (ImageButton) findViewById(R.id.faq_button);
        buttonFAQ.setVisibility(View.GONE);

        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toolbar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.headerkecil) );
        } else {
            toolbar.setBackground(ContextCompat.getDrawable(this, R.drawable.headerkecil));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
